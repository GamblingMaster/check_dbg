
// CheckDbgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "CheckDbg.h"
#include "CheckDbgDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#ifndef PAGE_SIZE
#define PAGE_SIZE 0x1000
#endif

#define PAGE_ALIGN(Va) ((PVOID)((ULONG_PTR)(Va) & ~(PAGE_SIZE - 1)))

#define INITIAL_RECORD_SIZE 1000

#define NtCurrentProcess() ((HANDLE)-1)

// CCheckDbgDlg 对话框
void get_all_privilege()
{
	using namespace ntdll;
	for (USHORT i = 0; i < 0x100; i++)
	{
		BOOLEAN Old;
		RtlAdjustPrivilege(i, TRUE, FALSE, &Old);
	}
}
void alloc_cmd_window()
{
	AllocConsole();
	SetConsoleTitle(_T("DbgLog"));
	AttachConsole(GetCurrentProcessId());

	FILE* pFile = nullptr;
	freopen_s(&pFile, "CON", "r", stdin);
	freopen_s(&pFile, "CON", "w", stdout);
	freopen_s(&pFile, "CON", "w", stderr);
}

bool GetProcessDataFromThread(_In_ DWORD ThreadId, _Out_ DWORD& ProcessId, _Out_writes_z_(MAX_PATH) wchar_t ProcessPath[MAX_PATH])
{
	bool status = false;

	ProcessId = 0;
	ProcessPath[0] = NULL;
	HANDLE Thread = OpenThread(THREAD_QUERY_LIMITED_INFORMATION, FALSE, ThreadId);
	if (Thread)
	{
		ProcessId = GetProcessIdOfThread(Thread);
		if (ProcessId)
		{
			HANDLE Process = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, ProcessId);
			if (Process)
			{
				status = (GetModuleFileNameExW(Process, NULL, ProcessPath, MAX_PATH) != 0);
				CloseHandle(Process);
			}
		}
		CloseHandle(Thread);
	}
	return status;
}



CCheckDbgDlg::CCheckDbgDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CHECKDBG_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCheckDbgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCheckDbgDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CCheckDbgDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CCheckDbgDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CCheckDbgDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CCheckDbgDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CCheckDbgDlg 消息处理程序

BOOL CCheckDbgDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	get_all_privilege();
	alloc_cmd_window();
	
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CCheckDbgDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CCheckDbgDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

int check()
{
	int status = -1;
	BYTE _LongInt3[] = { 0xCD, 0x03 };
	PBYTE Memory = (PBYTE)VirtualAlloc(NULL, (PAGE_SIZE * 2), (MEM_COMMIT | MEM_RESERVE), PAGE_EXECUTE_READWRITE);
	if (!Memory)
	{
		fprintf(stderr, "[-] ERROR: Failed to allocate memory.\n");
		goto Cleanup;
	}
	//	| Page 1						| Page 2
	//	[.....................0xCD 0x03][............................]

	PBYTE CodeLocation = &Memory[PAGE_SIZE - sizeof(_LongInt3)];
	PBYTE DeadPageLocation = &Memory[PAGE_SIZE];
	memcpy(CodeLocation, _LongInt3, sizeof(_LongInt3));
	PSAPI_WORKING_SET_EX_INFORMATION wsi;
	wsi.VirtualAddress = DeadPageLocation;
	if (!QueryWorkingSetEx(NtCurrentProcess(), &wsi, sizeof(wsi)))
	{
		fprintf(stderr, "[-] ERROR: QueryWorkingSetEx failed with error: 0x%lx.\n", GetLastError());
		goto Cleanup;
	}

	if (wsi.VirtualAttributes.Valid)
	{
		fprintf(stderr, "[-] ERROR: Page is expected to be invalid. Make sure you have not inadvertently accessed this page.\n");
		goto Cleanup;
	}

	__try
	{
		// Invoke the long form of int 3.
		((void(*)())CodeLocation)();
	}
	__except (EXCEPTION_EXECUTE_HANDLER) { }

	if (!QueryWorkingSetEx(NtCurrentProcess(), &wsi, sizeof(wsi)))
	{
		fprintf(stderr, "[-] ERROR: QueryWorkingSetEx failed with error: 0x%lx.\n", GetLastError());
		goto Cleanup;
	}

	printf("[+] Debugger %s.\n", ((wsi.VirtualAttributes.Valid) ? "detected" : "not detected"));

	status = (wsi.VirtualAttributes.Valid);

Cleanup:

	// Free allocated memory.
	if (Memory)
	{
		VirtualFree(Memory, 0, MEM_FREE);
		Memory = NULL;
	}

	return status;

}

void CCheckDbgDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	auto st = check();
	if (st>0)
	{
		AfxMessageBox(_T("发现调试"));
	}
}

int check_ce()
{
	int status = -1;

	PBYTE AllocatedBuffer = NULL;
	PPSAPI_WS_WATCH_INFORMATION_EX WatchInfoEx = NULL;

	const DWORD CurrentProcessId = GetCurrentProcessId();
	printf("[+] PID: %lu\n", CurrentProcessId);


#if defined(_M_IX86)
	// Can't run on Wow64 (32-bit on 64-bit OS).
	BOOL Wow64Process = FALSE;
	if (IsWow64Process(NtCurrentProcess(), &Wow64Process) && Wow64Process)
	{
		fprintf(stderr, "[-] ERROR: This process cannot be run under Wow64.\n");
		goto Cleanup;
	}
#endif

	// Initiate monitoring of the working set for this process.
	if (!InitializeProcessForWsWatch(NtCurrentProcess()))
	{
		fprintf(stderr, "[-] ERROR: Failed to initialize process for working set watch. InitializeProcessForWsWatch failed with error: %lu.\n", GetLastError());
		goto Cleanup;
	}
	AllocatedBuffer = (PBYTE)VirtualAlloc(NULL, PAGE_SIZE, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (!AllocatedBuffer)
	{
		fprintf(stderr, "[-] ERROR: Failed to allocate %u bytes for page faulting test buffer.\n", PAGE_SIZE);
		goto Cleanup;
	}

	printf("[+] Allocated buffer at 0x%p.\n", AllocatedBuffer);

	DWORD WatchInfoSize = (sizeof(PSAPI_WS_WATCH_INFORMATION_EX) * INITIAL_RECORD_SIZE);
	WatchInfoEx = (PPSAPI_WS_WATCH_INFORMATION_EX)malloc(WatchInfoSize);
	if (!WatchInfoEx)
	{
		fprintf(stderr, "[-] ERROR: Failed to allocate %lu bytes.\n", WatchInfoSize);
		goto Cleanup;
	}

	while (TRUE)
	{
		memset(WatchInfoEx, 0, WatchInfoSize);

		if (!GetWsChangesEx(NtCurrentProcess(), WatchInfoEx, &WatchInfoSize))
		{
			DWORD ErrorCode = GetLastError();

			// This really isn't an error. This just means that no new pages
			// have been mapped into our process' VA since the last time
			// we called GetWsChangesEx.
			if (ErrorCode == ERROR_NO_MORE_ITEMS)
			{
				// Wait a little bit before trying again.
				Sleep(1);
				continue;
			}

			// Any other error code is bad.
			if (ErrorCode != ERROR_INSUFFICIENT_BUFFER)
			{
				fprintf(stderr, "[-] ERROR: GetWsChangesEx failed with error: %lu.\n", ErrorCode);
				goto Cleanup;
			}

			// If we get this far, we need to increase the buffer size. 
			WatchInfoSize *= 2;

			free(WatchInfoEx);
			WatchInfoEx = (PPSAPI_WS_WATCH_INFORMATION_EX)malloc(WatchInfoSize);

			if (!WatchInfoEx)
			{
				fprintf(stderr, "[-] ERROR: Failed to allocate %lu bytes.\n", WatchInfoSize);
				goto Cleanup;
			}

			continue;
		}

		bool bFound = false;
		for (size_t i = 0;; ++i)
		{
			PPSAPI_WS_WATCH_INFORMATION_EX info = &WatchInfoEx[i];

			
			if (info->BasicInfo.FaultingPc == NULL)
				break;

			PVOID FaultingPageVa = PAGE_ALIGN(info->BasicInfo.FaultingVa);
			if (FaultingPageVa == AllocatedBuffer)
			{
				printf("[+] 0x%p (0x%p) was mapped by 0x%p (TID: %lu).\n", FaultingPageVa, info->BasicInfo.FaultingVa, info->BasicInfo.FaultingPc, (DWORD)info->FaultingThreadId);

				DWORD ProcessId;
				wchar_t ProcessPath[MAX_PATH];

				if (GetProcessDataFromThread((DWORD)info->FaultingThreadId, ProcessId, ProcessPath))
					printf("\t--> %S (PID: %lu).\n", ProcessPath, ProcessId);

				
				bFound = true;
				break;
			}
		}

		if (bFound)
		{
			status = 1;
			break;
		}
	}

Cleanup:
	// 'free' the 'malloc's.
	if (WatchInfoEx)
	{
		free(WatchInfoEx);
		WatchInfoEx = NULL;
	}

	if (AllocatedBuffer)
	{
		VirtualFree(AllocatedBuffer, 0, MEM_RELEASE);
		AllocatedBuffer = NULL;
	}

	return status;
}


void CCheckDbgDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	if (check_ce()>0)
	{
		AfxMessageBox(_T("有人读了我的内存"));
	}
}


void CCheckDbgDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
}

static inline int rdtsc_diff() {
	uint64_t ret, ret2;
	ret = __rdtsc();
	ret2 = __rdtsc();
//	fprintf(stdout, "(%llu - %llu)", ret, ret2);
	return ret2 - ret;
}

void check4() {
	int i, avg = 0, sub;
	auto T1 = GetTickCount();
	for (i = 0; i < 1000; i++) {
		sub = rdtsc_diff();
		//fprintf(stdout, "rdtsc difference: %d\n", sub);
		avg = avg + sub;
		Sleep(1);
	}
	auto T2 = GetTickCount();
	fprintf(stdout, "difference average is: %d %d\n", (avg / 1000),T2-T1);
	auto speed = (T2 - T1) / 1000;
	if (speed>1)
	{
		//再慢也不可能2秒才运行完
		fprintf(stdout, "VM detected\r\n");
	}
	__try
	{
		//测试不求行的sidt虚化，比如hyperplatform这种
		//理论上应该用随便给个invalid地址，还可以随便给个内核地址（某抄来的代码会炸飞天）
		__sidt((void*)(0));
	}
	__except (1)
	{

	}
	return;
}

void CCheckDbgDlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	check4();
}
